package com.geogames.authenticationservice.service;

import com.geogames.authenticationservice.data.RegisterUserCredentials;
import com.geogames.authenticationservice.data.entity.PasswordEntity;
import com.geogames.authenticationservice.data.entity.UserEntity;
import com.geogames.authenticationservice.data.repository.PasswordRepository;
import com.geogames.authenticationservice.data.repository.UserRepository;
import com.geogames.authenticationservice.exception.PasswordTooWeakException;
import com.geogames.authenticationservice.exception.UsernameOrEmailIsNotUniqueException;
import com.geogames.authenticationservice.util.HashType;
import com.geogames.authenticationservice.util.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserRegistrationServiceTest {

    private static final Integer UNIQUE_USER_ID = 100;
    private static final String UNIQUE_USERNAME = "janeDoe";
    private static final String UNIQUE_EMAIL = "jane.doe@test.com";
    private static final String WEAK_PASSWORD = "123";
    private static final String PASSWORD = "Test1234";
    //    Bcrypt hash for : Test1234
    private static final String PASSWORD_HASH = "$2a$10$IGee4maEkjtFL.P1qYV.bOupvt/XMqabPCX7mSq8ulF46oNhU4VB6";

    @Mock
    UserRepository userRepository;

    @Mock
    PasswordRepository passwordRepository;

    UserRegistrationService userRegistrationService;

    @Before
    public void setUp() throws Exception {
        userRegistrationService = new UserRegistrationService(userRepository, passwordRepository, new BCryptPasswordEncoder());
    }

    @Test
    public void givenUniqueUsernameStrongPasswordAndUniqueEmail_whenCreateUserIsCalled_userIsCreated() throws Exception {
//        given
        RegisterUserCredentials userCredentials = new RegisterUserCredentials(UNIQUE_USERNAME, PASSWORD, UNIQUE_EMAIL);
        PasswordEntity password = new PasswordEntity(100, UNIQUE_USER_ID, PASSWORD_HASH, HashType.BCrypt, new Timestamp(System.currentTimeMillis()));
        UserEntity testUser = new UserEntity(UNIQUE_USER_ID, UNIQUE_USERNAME, "john.doe@test.com", Role.USER, Arrays.asList(password));

        when(userRepository.findByUsernameOrEmailAllIgnoreCase(UNIQUE_USERNAME, UNIQUE_EMAIL)).thenReturn(Optional.empty());
        when(userRepository.save(new UserEntity(UNIQUE_USERNAME, UNIQUE_EMAIL))).thenReturn(testUser);

//        when
        UserEntity userEntity = userRegistrationService.createUser(userCredentials);

//        then
        assertEquals(testUser, userEntity);
        assertEquals(testUser.getPasswordLog().get(0).getPasswordHash(), PASSWORD_HASH);
        verify(userRepository, Mockito.times(1)).save(any(UserEntity.class));
        verify(passwordRepository, Mockito.times(1)).save(any(PasswordEntity.class));
    }

    @Test(expected = UsernameOrEmailIsNotUniqueException.class)
    public void givenNonUniqueUsernameStrongPasswordAndUniqeEmail_whenCreateUserIsCalled_exceptionIsThrown() throws Exception {
//        given
        RegisterUserCredentials userCredentials = new RegisterUserCredentials(UNIQUE_USERNAME, PASSWORD, UNIQUE_EMAIL);
        PasswordEntity password = new PasswordEntity(100, UNIQUE_USER_ID, PASSWORD_HASH, HashType.BCrypt, new Timestamp(System.currentTimeMillis()));
        UserEntity testUser = new UserEntity(UNIQUE_USER_ID, UNIQUE_USERNAME, "john.doe@test.com", Role.USER, Arrays.asList(password));

        when(userRepository.findByUsernameOrEmailAllIgnoreCase(UNIQUE_USERNAME, UNIQUE_EMAIL)).thenReturn(Optional.of(testUser));

//        when
        userRegistrationService.createUser(userCredentials);
    }

    @Test(expected = PasswordTooWeakException.class)
    public void givenUniqueUsernameWeakPasswordAndUniqueEmail_whenCreateUserIsCalled_exceptionIsThrown() throws Exception {
//        given
        RegisterUserCredentials userCredentials = new RegisterUserCredentials(UNIQUE_USERNAME, WEAK_PASSWORD, UNIQUE_EMAIL);

        when(userRepository.findByUsernameOrEmailAllIgnoreCase(UNIQUE_USERNAME, UNIQUE_EMAIL)).thenReturn(Optional.empty());

//        when
        userRegistrationService.createUser(userCredentials);

//        then
        verify(userRepository, Mockito.never()).save(any(UserEntity.class));
        verify(passwordRepository, Mockito.never()).save(any(PasswordEntity.class));
    }

}