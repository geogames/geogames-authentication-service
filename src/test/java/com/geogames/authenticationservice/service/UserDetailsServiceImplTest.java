package com.geogames.authenticationservice.service;

import com.geogames.authenticationservice.data.entity.PasswordEntity;
import com.geogames.authenticationservice.data.entity.UserEntity;
import com.geogames.authenticationservice.data.repository.UserRepository;
import com.geogames.authenticationservice.util.HashType;
import com.geogames.authenticationservice.util.Role;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

    //    Bcrypt hash for : Test1234
    private static final String PASSWORD_HASH = "$2a$10$IGee4maEkjtFL.P1qYV.bOupvt/XMqabPCX7mSq8ulF46oNhU4VB6";
    private static final String USERNAME = "johndoe";
    private final Integer USER_ID = 100;

    @Mock
    UserRepository userRepository;

    UserDetailsServiceImpl userDetailsService;

    @Before
    public void setUp() throws Exception {
        userDetailsService = new UserDetailsServiceImpl(userRepository);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsernameIsCalledForNonExistingUser_ExceptionShouldBeThrown() {
//        given
        String nonExistingUsername = "uncleBob";
        when(userRepository.findByUsernameIgnoreCase(nonExistingUsername)).thenReturn(Optional.empty());

//        when
        userDetailsService.loadUserByUsername(nonExistingUsername);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void loadUserByUsernameIsCalledForAUserWithoutAPassword_ExceptionShouldBeThrown() {
//        given
        UserEntity testUser = new UserEntity(USER_ID, USERNAME, "john.doe@test.com", Role.USER, new ArrayList<>());
        when(userRepository.findByUsernameIgnoreCase(USERNAME)).thenReturn(Optional.of(testUser));

//        when
        userDetailsService.loadUserByUsername(USERNAME);
    }

    @Test
    public void loadUserByUsernameIsCalledForAnExistingUserWithOnlyOnePasswordInLog() {
//        given
        PasswordEntity password = new PasswordEntity(100, USER_ID, PASSWORD_HASH, HashType.BCrypt, new Timestamp(System.currentTimeMillis()));
        UserEntity testUser = new UserEntity(USER_ID, USERNAME, "john.doe@test.com", Role.USER, Arrays.asList(password));
        when(userRepository.findByUsernameIgnoreCase(USERNAME)).thenReturn(Optional.of(testUser));

//        when
        UserDetails userDetails = userDetailsService.loadUserByUsername(USERNAME);

//        then
        assertEquals(userDetails.getUsername(), USERNAME);
        assertEquals(userDetails.getPassword(), PASSWORD_HASH);
        assertEquals(userDetails.getAuthorities().toString(), Collections.unmodifiableSet(new HashSet<>(Arrays.asList("ROLE_USER"))).toString());
    }
}