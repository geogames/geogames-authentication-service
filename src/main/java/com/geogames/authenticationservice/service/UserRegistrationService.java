package com.geogames.authenticationservice.service;

import com.geogames.authenticationservice.data.RegisterUserCredentials;
import com.geogames.authenticationservice.data.entity.PasswordEntity;
import com.geogames.authenticationservice.data.entity.UserEntity;
import com.geogames.authenticationservice.data.repository.PasswordRepository;
import com.geogames.authenticationservice.data.repository.UserRepository;
import com.geogames.authenticationservice.exception.PasswordTooWeakException;
import com.geogames.authenticationservice.exception.UsernameOrEmailIsNotUniqueException;
import com.geogames.authenticationservice.util.HashType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

@Service
public class UserRegistrationService {
    private UserRepository userRepository;
    private PasswordRepository passwordRepository;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserRegistrationService(UserRepository userRepository, PasswordRepository passwordRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordRepository = passwordRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public UserEntity createUser(RegisterUserCredentials userCredentials) throws UsernameOrEmailIsNotUniqueException, PasswordTooWeakException {
        if (userRepository
                .findByUsernameOrEmailAllIgnoreCase(userCredentials.getUsername(), userCredentials.getEmail())
                .isPresent()) {
            throw new UsernameOrEmailIsNotUniqueException(UsernameOrEmailIsNotUniqueException.NOT_UNIQUE);
        }
        if (userCredentials.getPassword().length() < 8) {
            throw new PasswordTooWeakException(PasswordTooWeakException.PASSWORD_TOO_WEAK);
        }

        UserEntity userEntity = userRepository.save(new UserEntity(userCredentials.getUsername(), userCredentials.getEmail()));

        passwordRepository.save(
                new PasswordEntity(userEntity.getUserId(), passwordEncoder.encode(userCredentials.getPassword()),
                        HashType.BCrypt, new Timestamp(System.currentTimeMillis()))
        );

        return userEntity;
    }
}
