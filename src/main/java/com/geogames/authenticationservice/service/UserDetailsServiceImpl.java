package com.geogames.authenticationservice.service;

import com.geogames.authenticationservice.data.entity.PasswordEntity;
import com.geogames.authenticationservice.data.entity.UserEntity;
import com.geogames.authenticationservice.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository
                .findByUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));

        PasswordEntity mostRecentPassword = user.getPasswordLog()
                .stream()
                .sorted(Comparator.comparing(PasswordEntity::getDateSet))
                .findFirst()
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " does not have a password"));

        List<GrantedAuthority> grantedAuthority =
                AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_" + user.getUserRole().toString());

        return new User(user.getUsername(), mostRecentPassword.getPasswordHash(), grantedAuthority);
    }
}
