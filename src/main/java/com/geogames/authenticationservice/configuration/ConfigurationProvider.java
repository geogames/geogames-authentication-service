package com.geogames.authenticationservice.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationProvider {
    @Value("${security.jwt.uri}")
    private String authenticationUri;

    @Value("${security.jwt.header}")
    private String authenticationHeader;

    @Value("${security.jwt.expiration}")
    private int JWTExpirationTime;

    public String getAuthenticationUri() {
        return authenticationUri;
    }

    public String getAuthenticationHeader() {
        return authenticationHeader;
    }

    public int getJWTExpirationTime() {
        return JWTExpirationTime;
    }
}