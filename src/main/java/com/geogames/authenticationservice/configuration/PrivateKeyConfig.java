package com.geogames.authenticationservice.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

@Component
public class PrivateKeyConfig {

    @Value("${security.jwt.private.key.filepath}")
    private String privateKeyFilepath;

    private PrivateKey privateKey;

    @PostConstruct
    private void init() throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(privateKeyFilepath));

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        privateKey = kf.generatePrivate(spec);
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
