package com.geogames.authenticationservice.data.entity;

import com.geogames.authenticationservice.util.HashType;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "password_log")
public class PasswordEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "passwordLogSequence")
    @SequenceGenerator(name = "passwordLogSequence", sequenceName = "password_log_sequence")
    @Column(name = "password_id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @Column(name = "password_hash", nullable = false)
    private String passwordHash;

    @Column(name = "hash_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private HashType hashType;

    @Column(name = "date_set", nullable = false)
    private Timestamp dateSet;

    public PasswordEntity() {
    }

//    For testing purposes only.
    public PasswordEntity(Integer id, Integer userId, String passwordHash, HashType hashType, Timestamp dateSet) {
        this.id = id;
        this.userId = userId;
        this.passwordHash = passwordHash;
        this.hashType = hashType;
        this.dateSet = new Timestamp(dateSet.getTime());;
    }

    public PasswordEntity(Integer userId, String passwordHash, HashType hashType, Timestamp dateSet) {
        this.userId = userId;
        this.passwordHash = passwordHash;
        this.hashType = hashType;
        this.dateSet = new Timestamp(dateSet.getTime());
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public HashType getHashType() {
        return hashType;
    }

    public Timestamp getDateSet() {
        return new Timestamp(dateSet.getTime());
    }
}
