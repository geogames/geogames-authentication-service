package com.geogames.authenticationservice.data.entity;

import com.geogames.authenticationservice.util.Role;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "usersSequence")
    @SequenceGenerator(name = "usersSequence", sequenceName = "users_sequence")
    @Column(name = "user_id", unique = true, nullable = false)
    private Integer userId;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "email", unique = true, nullable = false)
    @Email
    private String email;

    @Column(name = "user_role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role userRole;

    @OneToMany(mappedBy = "userId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<PasswordEntity> passwordLog;

    @PrePersist
    void preInsert(){
        if (this.userRole == null){
            this.userRole = Role.USER;
        }
    }

    public UserEntity() {
    }

    //    For testing purposes only.
    public UserEntity(Integer userId, String username, String email, Role userRole, List<PasswordEntity> passwordLog) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.userRole = userRole;
        this.passwordLog = passwordLog;
    }

    public UserEntity(String username, String email) {
        this.username = username;
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserEntity)) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(username, that.username) &&
                Objects.equals(email, that.email) &&
                userRole == that.userRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, userRole);
    }

    public Integer getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Role getUserRole() {
        return userRole;
    }

    public List<PasswordEntity> getPasswordLog() {
        return passwordLog;
    }
}
