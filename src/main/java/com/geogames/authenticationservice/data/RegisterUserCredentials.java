package com.geogames.authenticationservice.data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class RegisterUserCredentials {
    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    @Email
    private String email;

    public RegisterUserCredentials(){};

    public RegisterUserCredentials(@NotNull String username, @NotNull String password, @NotNull @Email String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
