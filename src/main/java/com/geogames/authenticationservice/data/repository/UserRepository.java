package com.geogames.authenticationservice.data.repository;

import com.geogames.authenticationservice.data.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    Optional<UserEntity> findByUsernameIgnoreCase(String username);
    Optional<UserEntity> findByUsernameOrEmailAllIgnoreCase(String username, String email);
}
