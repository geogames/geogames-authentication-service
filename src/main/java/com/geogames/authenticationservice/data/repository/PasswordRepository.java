package com.geogames.authenticationservice.data.repository;

import com.geogames.authenticationservice.data.entity.PasswordEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PasswordRepository extends PagingAndSortingRepository<PasswordEntity, Integer> {
}
