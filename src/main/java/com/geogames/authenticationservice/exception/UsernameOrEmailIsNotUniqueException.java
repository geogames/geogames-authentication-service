package com.geogames.authenticationservice.exception;

public class UsernameOrEmailIsNotUniqueException extends Exception {
    public static final String NOT_UNIQUE = "An account with this username or email already exists.";

    public UsernameOrEmailIsNotUniqueException(String message) {
        super(message);
    }
}
