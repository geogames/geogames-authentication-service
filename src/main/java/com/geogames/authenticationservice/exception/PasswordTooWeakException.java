package com.geogames.authenticationservice.exception;

public class PasswordTooWeakException extends Exception {
    public static final String PASSWORD_TOO_WEAK = "Provide a password with a minimum length of 8 characters.";

    public PasswordTooWeakException(String message){
        super(message);
    }
}
