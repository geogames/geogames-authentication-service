package com.geogames.authenticationservice.web;

import com.geogames.authenticationservice.data.RegisterUserCredentials;
import com.geogames.authenticationservice.exception.PasswordTooWeakException;
import com.geogames.authenticationservice.exception.UsernameOrEmailIsNotUniqueException;
import com.geogames.authenticationservice.service.UserRegistrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@Api(value = "userAccount", description = "Operations for managing a user's account")
public class UserRegistrationController {
    static final Logger LOGGER = LoggerFactory.getLogger(UserRegistrationController.class);
    @Autowired
    UserRegistrationService userRegistrationService;

    @RequestMapping(value = "/auth/register", consumes = "application/json", method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Create a new account", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User has been successfully registered."),
            @ApiResponse(code = 400, message = "The password needs to be at lest 8 characters long." +
                    "If the password is correct then either username or email are already in use."),
    }
    )
    public ResponseEntity registerUser(@RequestBody RegisterUserCredentials registerUserCredentials) {
        LOGGER.debug("Received a registration request for a user: {}", registerUserCredentials.getUsername());
        try {
            userRegistrationService.createUser(registerUserCredentials);
        } catch (UsernameOrEmailIsNotUniqueException | PasswordTooWeakException ex) {

            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(new JSONObject()
                            .put("status", "failure")
                            .put("message", ex.getMessage())
                            .toString(2));
        }
        return ResponseEntity
                .ok(new JSONObject()
                        .put("status", "success")
                        .put("message", "User has been successfully registered.")
                        .toString(2));

    }
}
