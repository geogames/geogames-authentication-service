package com.geogames.authenticationservice.util;

public enum Role {
    USER,
    ADMIN
}
